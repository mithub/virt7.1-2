#!/bin/bash

sudo apt -y update && sudo apt -y upgrade

# sudo hostnamectl set-hostname virt.local --static
sudo hostnamectl set-hostname HOSTNAME --static

# sudo vim /etc/hosts
# 192.168.2.21	virt.local virt
# hostname --ip-address == 192.168.2.21

echo "deb http://download.proxmox.com/debian/pve bullseye pve-no-subscription" | sudo tee /etc/apt/sources.list.d/pve-install-repo.list

wget http://download.proxmox.com/debian/proxmox-release-bullseye.gpg
sudo mv proxmox-release-bullseye.gpg /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg
chmod +r /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg

apt update
sudo apt full-upgrade -y

echo "deb http://download.proxmox.com/debian/ceph-pacific bullseye main" | sudo tee /etc/apt/sources.list.d/ceph.list


sudo apt update
sudo apt install proxmox-ve postfix open-iscsi
