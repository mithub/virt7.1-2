#!/bin/bash

mv /usr/share/pve-docs/ /usr/share/noshow/

sed -i.original -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service

sed -i.original "s/quiet/quiet intel_iommu=on/g" /etc/default/grub

cat <<EOF > /etc/modules
vfio
vfio_iommu_type1
vfio_pci
vfio_virqfd
EOF

sed -i.original "s/Proxmox VE/Virtual Server/g" /etc/default/grub.d/proxmox-ve.cfg

cp /usr/bin/pvebanner /usr/bin/pvebanner.original
cat <<EOF > /usr/bin/pvebanner
#!/usr/bin/perl

use strict;
use PVE::INotify;
use PVE::Cluster;

my $nodename = PVE::INotify::nodename();
my $localip = PVE::Cluster::remote_node_ip($nodename, 1);

my $xline = '-' x 78;

my $banner = '';

if ($localip) {
    $banner .= <<__EOBANNER;

$xline
Welcome to the Virtual Environment.
$xline

__EOBANNER

}

open(ISSUE, ">/etc/issue");

print ISSUE $banner;

close(ISSUE);

exit (0);
EOF

mv /usr/share/pve-manager/js/pvemanagerlib.js /usr/share/pve-manager/js/pvemanagerlib.js.original
wget https://gitlab.com/mithub/virt7.1-2/-/raw/main/pvemanagerlib.js -P /usr/share/pve-manager/js

mv /usr/share/qemu-server/bootsplash.jpg /usr/share/qemu-server/bootsplash.jpg.original
wget https://gitlab.com/mithub/virt7.1-2/-/raw/main/bootsplash.jpg -P /usr/share/qemu-server

update-grub
