#!/bin/bash

apt update

apt install -y at vim mlocate net-tools zip rkhunter curl wget libsasl2-modules aria2 sudo

mv /usr/share/pve-docs/ /usr/share/noshow/

sed -i.original -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service

sed -i.original "s/quiet/quiet intel_iommu=on/g" /etc/default/grub
cat <<EOF > /etc/modules
vfio
vfio_iommu_type1
vfio_pci
vfio_virqfd
EOF

sed -i.original "s/Proxmox/Virtual Server/g;/8006/d;/config/d" /usr/bin/pvebanner

sed -i.original "s/Proxmox VE/Virtual Server/g" /etc/default/grub.d/proxmox-ve.cfg
# /etc/default/grub.d/proxmox-ve.cfg
# GRUB_DISTRIBUTOR="Virtual"
# GRUB_DISABLE_OS_PROBER=true

update-grub

curl -s https://install.zerotier.com | bash
